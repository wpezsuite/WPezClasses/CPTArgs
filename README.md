## WPezClasses: Custom Post Type Args

__Create your WordPress custom post type args The ezWay.__
   

### Simple Example

See the full example in the CPTRegister README. Link in the Helpful Links (below).

_Recommended: Use the WPezClasses autoloader (link below)._


There are four public methods:

- loadArgsBase( $arr ) - Allows you to build a library of standard CPT types / configs

- loadArgs( $arr ) - The particulars of this CTP

- updateArgs( $arr ) - Another way to load your args.

- getArgsAll( $arr ) - returns array_merge() of ArgsDefaults, ArgsBase, ArrArgs and the $arr passed in. 

The general idea is to give you as much granular control as possible, without having to slow you down with doing your own array_merge()s.


### FAQ

__1) Why?__

In this case, that's a really good question. The CPT args are already an array. Perhaps this class isn't all that necessary. None the less, it's self-documenting so referencing the codex for any details is no longer necessary.    


__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

- https://gitlab.com/wpezsuite/WPezClasses/WPezAutoload

- https://gitlab.com/wpezsuite/WPezClasses/CPTLabelsUS

- https://gitlab.com/wpezsuite/WPezClasses/CPTRegister

- https://codex.wordpress.org/Function_Reference/register_post_type



### TODO

- 


### CHANGE LOG

- v0.0.3 - Sun 8 Jan 2023
  - Minor touch-ups, no bump up in version

- v0.0.3 - Thur 1 December 2022
  - Refactoring + doc comments

- v0.0.2 - Saturday 27 April 2019
    - ADDED: template and template_lock for Gutenberg

- v0.0.1 - Monday 15 April 2019
    - This has been in development for quite some time, but not independently repo'ed until now. Please pardon the delay. 