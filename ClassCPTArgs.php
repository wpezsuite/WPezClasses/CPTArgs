<?php
/**
 * Class CPTArgs
 *
 * @package WPezSuite\WPezClasses\CPTArgs
 */

namespace WPezSuite\WPezClasses\CPTArgs;

// No WP? No go.
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

/**
 * Helper class for setting the args for a CPT.
 */
class ClassCPTArgs {


	/**
	 * The (slug) name of the CPT.
	 *
	 * @var string
	 */
	protected $str_post_type;

	/**
	 * An array of the args that array_merge()s over the defaults. Perhaps you have a library of these?
	 *
	 * @var array
	 */
	protected $arr_args_base;

	/**
	 * An array of the args that array_merge()s over the $arr_args_bases and the defaults. Probably unique to each CPT.
	 *
	 * @var array
	 */
	protected $arr_args;

	/**
	 * Setters return nothing but the result of a given set is available via this property and getSetReturn().
	 *
	 * @var mixed
	 */
	protected $mix_set_return;

	/**
	 * Name of the post type shown in the menu. Usually plural.
	 *
	 * @var string
	 */
	protected $str_label_plural;

	/**
	 * An array of labels. See: https:// developer.wordpress.org/reference/functions/get_post_type_labels/
	 *
	 * @var array
	 */
	protected $arr_labels;

	/**
	 * The CPT description.
	 *
	 * @var string
	 */
	protected $str_description;

	/**
	 * Default: false - Whether a post type is intended for use publicly either via the admin interface or by front - end users.
	 * While the default settings of $exclude_from_search, $publicly_queryable, $show_ui, and $show_in_nav_menus are
	 * inherited from $public, each does not rely on this relationship and controls a very specific intention.
	 *
	 * @var bool
	 */
	protected $bool_public;

	/**
	 * Default: false - Whether the post type is hierarchical(e.g., page).
	 *
	 * @var bool
	 */
	protected $bool_hierarchical;

	/**
	 * Default: opposite value of $public. Whether to exclude posts with this post type from front end search results.
	 *
	 * @var bool
	 */
	protected $bool_exclude_from_search;

	/**
	 * Default: inherited from $public - Whether queries can be performed on the front end for the post type as part of parse_request().
	 * Endpoints would require: * ? post_type = {post_type_key} * ? {post_type_key} = {single_post_slug} * ? {post_type_query_var} = {single_post_slug}.
	 *
	 * @var bool
	 */
	protected $bool_publicly_queryable;

	/**
	 * Default: value of $public. Whether to generate and allow a UI for managing this post type in the admin.
	 *
	 * @var bool
	 */
	protected $bool_show_ui;

	/**
	 * Default: value of $show_ui - Where to show the post type in the admin menu. To work, $show_ui must be true. If true, the post type is shown in its own top level menu.
	 * If false, no menu is shown. If a string of an existing top level menu ('tools.php' or 'edit.php?post_type=page', for example), the post type will be placed as a sub-menu of that.
	 *
	 * @var bool
	 */
	protected $bool_show_in_menu;

	/**
	 * Default: value of $public. Makes this post type available for selection in navigation menus.
	 *
	 * @var bool
	 */
	protected $bool_show_in_nav_menus;

	/**
	 * Default: value of $show_ui. Makes this post type available via the admin bar.
	 *
	 * @var bool
	 */
	protected $bool_show_in_admin_bar;

	/**
	 * Default: 30 (below Comments @ 25). The position in the menu order the post type should appear. show_in_menu must be true.
	 *
	 * @var int
	 */
	protected $int_menu_position;

	/**
	 * Default: posts icon - The url to the icon to be used for this menu or the name of the icon from the iconfont.
	 *
	 * @var string
	 */
	protected $str_menu_icon;

	/**
	 * Default: 'post'- The string to use to build the read, edit, and delete capabilities.
	 *
	 * @var string|array
	 */
	protected $mix_capability_type;

	/**
	 * An array of the capabilities for this post type.
	 *
	 * @var array
	 */
	protected $arr_capabilities;

	/**
	 * Default: null - Whether to use the internal default meta capability handling.
	 *
	 * @var bool
	 */
	protected $bool_map_meta_cap;


	/**
	 * An alias for calling add_post_type_support() directly. As of 3.5, boolean false can be passed as value instead of an array to prevent default (title and editor) behavior.
	 *
	 * @var array|bool
	 */
	protected $mix_supports;

	/**
	 * Default: none - Provide a callback function that will be called when setting up the meta boxes for the edit form.
	 *
	 * @var string
	 */
	protected $str_register_meta_box_cb;

	/**
	 * Default: no taxonomies - An array of registered taxonomies like category or post_tag that will be used with this post type.
	 *
	 * @var array
	 */
	protected $arr_taxonomies;

	/**
	 * Enables post type archives . Will use $post_type as archive slug by default.
	 *
	 * @var bool|string
	 */
	protected $mix_has_archive;

	/**
	 * Default: true (and uses $post_type as slug) - Triggers the handling of rewrites for this post type. To prevent rewrites, set to false.
	 *
	 * @var bool|array
	 */
	protected $mix_rewrite;

	/**
	 * Default: EP_PERMALINK - The default rewrite endpoint bitmasks.
	 *
	 * @var string
	 */
	protected $str_permalink_epmask;

	/**
	 * Default: true – set to $post_type - Sets the query_var key for this post type.
	 *
	 * @var bool|string
	 */
	protected $mix_query_var;

	/**
	 * Default: true - Can this post_type be exported.
	 *
	 * @var bool
	 */
	protected $bool_can_export;

	/**
	 * Default: null - Whether to delete posts of this type when deleting a user. If true, posts of this type belonging to the user will be moved to trash when then user is deleted.
	 *
	 * @var bool
	 */
	protected $bool_delete_with_user;

	/**
	 * Default: false - Whether to expose this post type in the REST API. Must be true to enable the Gutenberg editor.
	 *
	 * @var bool
	 */
	protected $bool_show_in_rest;

	/**
	 * Default: $post_type - The base slug that this post type will use when accessed using the REST API.
	 *
	 * @var string
	 */
	protected $str_rest_base;

	/**
	 * Default: WP_REST_Posts_Controller - An optional custom controller to use instead of WP_REST_Posts_Controller. Must be a subclass of WP_REST_Controller.
	 *
	 * @var string
	 */
	protected $str_rest_controller_class;

	/**
	 * Default: null - Templates can be declared in JS or in PHP as an array of blockTypes(block name and optional attributes).
	 *
	 * @var array
	 */
	protected $arr_template;

	/**
	 * Default: null (unknown) - Permitted values: 'contentOnly', 'all', "insert'.
	 *
	 * @var string
	 */
	protected $str_template_lock;


	/**
	 * The constructor.
	 *
	 * @param string $post_type The post type name/slug.
	 */
	public function __construct( string $post_type ) {

		$this->str_post_type = $post_type;
		$this->setPropertyDefaults();
	}

	/**
	 * Set the property defaults.
	 */
	protected function setPropertyDefaults() {

		// These are props for making this ez.
		$this->arr_args_base  = array();
		$this->arr_args       = array();
		$this->mix_set_return = null;

		// These props are for WP.
		$this->str_label_plural          = null;
		$this->arr_labels                = array();
		$this->str_description           = '';
		$this->bool_public               = false;
		$this->bool_exclude_from_search  = true;
		$this->bool_publicly_queryable   = null;
		$this->bool_show_ui              = false;
		$this->bool_show_in_nav_menus    = false;
		$this->bool_show_in_menu         = false;
		$this->bool_show_in_admin_bar    = null;
		$this->int_menu_position         = 30;
		$this->str_menu_icon             = null;
		$this->mix_capability_type       = 'post';
		$this->arr_capabilities          = null;
		$this->bool_map_meta_cap         = null;
		$this->bool_hierarchical         = false;
		$this->mix_supports              = array( 'title', 'editor' );
		$this->str_register_meta_box_cb  = null;
		$this->arr_taxonomies            = array();
		$this->mix_has_archive           = false;
		$this->mix_rewrite               = true;
		$this->str_permalink_epmask      = EP_PERMALINK;
		$this->mix_query_var             = true;
		$this->bool_can_export           = true;
		$this->bool_delete_with_user     = null;
		$this->bool_show_in_rest         = false;
		$this->str_rest_base             = null;
		$this->str_rest_controller_class = 'WP_REST_Posts_Controller';
		$this->arr_template              = null;
		$this->str_template_lock         = false;
	}


	/**
	 * Set the label plural.
	 *
	 * @param string $str_plural The label plural.
	 *
	 * @return void
	 */
	public function setLabel( string $str_plural = '' ) {

		$this->mix_set_return = $this->setString( 'str_label_plural', $str_plural );
	}

	/**
	 * Set the CTP's labels.
	 *
	 * @param array $arr The CPT's labels.
	 *
	 * @return void
	 */
	public function setLabels( array $arr = array() ) {

		$this->mix_set_return = $this->setArray( 'arr_labels', $arr );
	}

	/**
	 * Set the CTP's description arg's value.
	 *
	 * @param string $str The CPT's description.
	 *
	 * @return void
	 */
	public function setDescription( string $str = '' ) {

		$this->mix_set_return = $this->setString( 'str_description', $str );
	}

	/**
	 * Set the CPT's public arg's value.
	 *
	 * @param bool $bool The CPT's public arg value.
	 *
	 * @return void
	 */
	public function setPublic( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_public', $bool );
	}

	/**
	 * Set the CPT's exclude_from_search arg's value.
	 *
	 * @param bool $bool The CPT's exclude_from_search arg value.
	 *
	 * @return void
	 */
	public function setExcludeFromSearch( bool $bool = true ) {

		$this->mix_set_return = $this->setBool( 'bool_exclude_from_search', $bool );
	}


	/**
	 * Set the CPT's publicly_queryable arg's value.
	 *
	 * @param bool $bool The CPT's publicly_queryable arg value.
	 *
	 * @return void
	 */
	public function setPubliclyQueryable( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_publicly_queryable', $bool );
	}

	/**
	 * Set the CPT's show_ui arg's value.
	 *
	 * @param bool $bool The CPT's show_ui arg value.
	 *
	 * @return void
	 */
	public function setShowUI( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_show_ui', $bool );
	}

	/**
	 * Set the CPT's show_in_nav_menus arg's value.
	 *
	 * @param bool $bool The CPT's show_in_nav_menus arg value.
	 *
	 * @return void
	 */
	public function setShowInNavMenus( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_show_in_nav_menus', $bool );
	}

	/**
	 * Set the CPT's show_in_menu arg's value.
	 *
	 * @param bool $bool The CPT's show_in_menu arg value.
	 *
	 * @return void
	 */
	public function setShowInMenu( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_show_in_menu', $bool );
	}

	/**
	 * Set the CPT's show_in_admin_bar arg's value.
	 *
	 * @param bool $bool The CPT's show_in_admin_bar arg value.
	 *
	 * @return void
	 */
	public function setShowInAdminBar( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_show_in_admin_bar', $bool );
	}

	/**
	 * Set the CPT's menu_position arg's value.
	 *
	 * @param int $int The CPT's menu_position arg value.
	 *
	 * @return void
	 */
	public function setMenuPosition( int $int = 30 ) {

		$this->int_menu_position = absint( $int );
		$this->mix_set_return    = true;
	}

	/**
	 * Set the CPT's menu_icon arg's value. Ref: // https://developer.wordpress.org/resource/dashicons/ .
	 *
	 * @param string $str The CPT's menu_icon arg value.
	 *
	 * @return void
	 */
	public function setMenuIcon( string $str = '' ) {

		$this->mix_set_return = $this->setString( 'str_menu_icon', $str );
	}


	/**
	 * Set the CPT's capability_type arg's value.
	 *
	 * @param string|array $mix The CPT's capability_type arg value.
	 *
	 * @return void
	 */
	public function setCapabilityType( $mix = false ) {

		$this->mix_set_return = false;
		if ( is_string( $mix ) || is_array( $mix ) ) {

			$this->mix_capability_type = $mix;

			$this->mix_set_return = true;
		}
	}

	/**
	 * Set the CPT's capabilities arg's value.
	 *
	 * @param array $arr The CPT's capabilities arg value.
	 *
	 * @return void
	 */
	public function setCapabilities( array $arr = array() ) {

		$this->mix_set_return = $this->setArray( 'arr_capabilities', $arr );
	}

	/**
	 * Set the CPT's map_meta_cap arg's value.
	 *
	 * @param bool $bool The CPT's map_meta_cap arg value.
	 *
	 * @return void
	 */
	public function setMapMetaCap( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_map_meta_cap', $bool );
	}

	/**
	 * Set the CPT's hierarchical arg's value.
	 *
	 * @param bool $bool The CPT's hierarchical arg value.
	 *
	 * @return void
	 */
	public function setHierarchical( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_hierarchical', $bool );
	}

	/**
	 * Set the CPT's supports arg's value.
	 *
	 * @param array|false $mix The CPT's supports arg value.
	 *
	 * @return void
	 */
	public function setSupports( $mix = array( 'title', 'editor' ) ) {

		$this->mix_set_return = false;
		if ( is_array( $mix ) || false === $mix ) {
			$this->mix_supports = $mix;

			$this->mix_set_return = true;
		}
	}

	/**
	 * Set the CPT's register_meta_box_cb arg's value.
	 *
	 * @param string $str The CPT's register_meta_box_cb arg value.
	 *
	 * @return void
	 */
	public function setRegisterMetaBoxCB( string $str = '' ) {

		$this->mix_set_return = $this->setString( 'str_register_meta_box_cb', $str );
	}

	/**
	 * Set the CPT's taxonomies arg's value.
	 *
	 * @param array $arr The CPT's taxonomies arg value.
	 *
	 * @return void
	 */
	public function setTaxonomies( array $arr = array() ) {

		$this->mix_set_return = $this->setArray( 'arr_taxonomies', $arr );
	}

	/**
	 * Set the CPT's has_archive arg's value.
	 *
	 * @param bool|string $mix The CPT's has_archive arg value.
	 *
	 * @return void
	 */
	public function setHasArchive( $mix = false ) {

		$this->mix_set_return = false;
		if ( is_string( $mix ) || is_bool( $mix ) ) {
			$this->mix_has_archive = $mix;
			$this->mix_set_return  = true;
		}
	}

	/**
	 * Set the CPT's rewrite arg's value.
	 *
	 * @param string|bool $mix The CPT's rewrite arg value.
	 *
	 * @return void
	 */
	public function setRewrite( $mix = true ) {

		$this->mix_set_return = false;
		if ( is_string( $mix ) || is_bool( $mix ) ) {
			$this->mix_rewrite    = $mix;
			$this->mix_set_return = true;
		}
	}

	/**
	 * Set the CPT's permalink_epmask arg's value.
	 *
	 * @param string $str The CPT's permalink_epmask arg value.
	 *
	 * @return void
	 */
	public function setPermalinkEpmask( string $str = '' ) {

		$this->mix_set_return = $this->setString( 'str_permalink_epmask', $str );
	}

	/**
	 * Set the CPT's query_var arg's value.
	 *
	 * @param string|bool $mix The CPT's query_var arg value.
	 *
	 * @return void
	 */
	public function setQueryVar( $mix = true ) {

		$this->mix_set_return = false;
		if ( is_string( $mix ) || is_bool( $mix ) ) {

			$this->mix_query_var  = $mix;
			$this->mix_set_return = true;
		}
	}

	/**
	 * Set the CPT's can_export arg's value.
	 *
	 * @param bool $bool The CPT's can_export arg value.
	 *
	 * @return void
	 */
	public function setCanExport( bool $bool = true ) {

		$this->mix_set_return = $this->setBool( 'bool_can_export', $bool );
	}


	/**
	 * Set the CPT's delete_with_user arg's value.
	 *
	 * @param bool $bool The CPT's delete_with_user arg value.
	 *
	 * @return void
	 */
	public function setDeleteWithUser( bool $bool = null ) {

		$this->mix_set_return = $this->setBool( 'bool_delete_with_user', $bool );
	}

	/**
	 * Set the CPT's show_in_rest arg's value.
	 *
	 * @param bool $bool The CPT's show_in_rest arg value.
	 *
	 * @return void
	 */
	public function setShowInREST( bool $bool = false ) {

		$this->mix_set_return = $this->setBool( 'bool_show_in_rest', $bool );
	}


	/**
	 * Set the CPT's rest_base arg's value.
	 *
	 * @param string $str The CPT's rest_base arg value.
	 *
	 * @return void
	 */
	public function setRESTBase( string $str = '' ) {

		$this->mix_set_return = $this->setString( 'str_rest_base', $str );
	}

	/**
	 * Set the CPT's rest_controller_class arg's value.
	 *
	 * @param string $str The CPT's rest_controller_class arg value.
	 *
	 * @return void
	 */
	public function setRESTControllerClass( string $str = '' ) {

		$this->mix_set_return = $this->setString( 'str_rest_controller_class', $str );

	}

	/**
	 * Set the CPT's template arg's value.
	 *
	 * @param array $arr The CPT's template arg value.
	 *
	 * @return void
	 */
	public function setTemplate( array $arr = array() ) {

		$this->mix_set_return = $this->setArray( 'arr_template', $arr );
	}

	/**
	 * Set the CPT's template_lock arg's value.
	 *
	 * @param false|string $mix The CPT's template_lock arg value.
	 *
	 * @return void
	 */
	public function setTemplateLock( $mix = false ) {

		$this->mix_set_return = false;
		if ( is_string( $mix ) || false === $mix ) {

			$this->mix_query_var  = $mix;
			$this->mix_set_return = true;
		}

	}


	/**
	 * If you have your library of "base" args for some different types of CPT.
	 *
	 * @param array $arr An array of CPT args.
	 *
	 * @return void
	 */
	public function loadArgsBase( array $arr = array() ) {

		$this->mix_set_return = $this->setArray( 'arr_args_base', $arr );
	}


	/**
	 * Set the CPT's args.
	 *
	 * @param array $arr The CPT's args.
	 *
	 * @return void
	 */
	public function loadArgs( array $arr = array() ) {

		$this->mix_set_return = $this->setArray( 'arr_args', $arr );
	}


	/**
	 * Similar to base args but these array_merge() on top of the arr_args.
	 *
	 * @param array $arr The CPT's args.
	 *
	 * @return void
	 */
	public function updateArgs( array $arr = array() ) {

		$this->arr_args       = array_merge( $this->arr_args, $arr );
		$this->mix_set_return = true;
	}


	/**
	 * Get all the Args.
	 *
	 * @param array $arr The CPT's args added on top during the get.
	 *
	 * @return array
	 */
	public function getArgsAll( array $arr = array() ) {
		
		$args_all = array_merge( $this->getArgsDefaults(), $this->arr_args_base, $this->arr_args, $arr );

		// Filter null values from the associative array.
		$args_all = array_filter(
			$args_all,
			function( $value, $key ) {
				return null !== $value;
			},
			ARRAY_FILTER_USE_BOTH
		);
		
		return $args_all;
	}


	/**
	 * The default set of CPT args.
	 *
	 * @return array
	 */
	protected function getArgsDefaults() {

		$args = array(

			/**
			 * (string) - Name of the post type shown in the menu. Usually plural.
			 *
			 * Default: value of $labels['name'] .
			 */
			'label'                 => $this->str_label_plural,

			/**
			 * (array) (optional) - An array of labels for this post type . If not set, post labels are
			 * inherited for non - hierarchical types and page labels for hierarchical ones . See:
			 *
			 * Ref: https://developer.wordpress.org/reference/functions/get_post_type_labels/
			 *
			 * For a full list of supported labels.
			 */
			'labels'                => $this->arr_labels,

			/**
			 * (string) (optional) A short descriptive summary of what the post type is.
			 *
			 * Default: blank
			 */
			'description'           => $this->str_description,

			/**
			 * (boolean) Whether a post type is intended for use publicly either via the admin interface or by front - end users.
			 * While the default settings of $exclude_from_search, $publicly_queryable, $show_ui, and $show_in_nav_menus are inherited
			 * from $public, each does not rely on this relationship and controls a very specific intention.
			 *
			 * Default: false
			 */
			'public'                => $this->bool_public,

			/**
			 * (boolean) Whether the post type is hierarchical (e.g. page). Allows Parent to be specified.
			 * The 'supports' parameter should contain 'page-attributes' to show the parent select box on the editor page.
			 *
			 * Default: false
			 */
			'hierarchical'          => $this->bool_hierarchical,

			/**
			 * (boolean) (importance) Whether to exclude posts with this post type from front end search results.
			 *
			 * - 'true' - site/?s=search-term will not include posts of this post type.
			 *
			 * - 'false' - site/?s=search-term will include posts of this post type.
			 *
			 * Note: If you want to show the posts's list that are associated to taxonomy's terms, you must set
			 * exclude_from_search to false (i.e., for call site_domaine/?taxonomy_slug=term_slug or site_domaine/taxonomy_slug/term_slug).
			 * If you set to true, on the taxonomy page (ex: taxonomy.php) WordPress will not find your posts and/or pagination
			 * will make 404 error...
			 *
			 * Default: value of the opposite of $public argument
			 */
			'exclude_from_search'   => $this->bool_exclude_from_search,

			/**
			 * (boolean) - Whether queries can be performed on the front end for the post type as part of parse_request() .
			 * Endpoints would require : * ? post_type = {post_type_key} * ? {post_type_key} = {single_post_slug} * ? {post_type_query_var} = {single_post_slug} if not set,
			 * the default is inherited from $public.
			 */
			'publicly_queryable'    => $this->bool_publicly_queryable,

			/**
			 * (boolean) - Whether to generate and allow a UI for managing this post type in the admin.
			 *
			 * - 'false' - do not display a user-interface for this post type
			 *
			 * - 'true' - display a user-interface (admin panel) for this post type
			 *
			 * Default: value of $public argument
			 */
			'show_ui'               => $this->bool_show_ui,

			/**
			 * (bool | string) - Where to show the post type in the admin menu. To work, $show_ui must be true.
			 * If true, the post type is shown in its own top level menu.
			 * If false, no menu is shown.
			 * If a string of an existing top level menu ('tools.php' or 'edit.php?post_type=page', for example), the post type will be placed as a sub-menu of that.
			 *
			 * Default: value of $show_ui.
			 */
			'show_in_nav_menus'     => $this->bool_show_in_menu,

			/**
			 * Makes this post type available for selection in navigation menus.
			 *
			 * Default: value of $public.
			 */
			'show_in_menu'          => $this->bool_show_in_nav_menus,

			/**
			 * Makes this post type available via the admin bar.
			 *
			 * Default: value of $show_in_menu.
			 */
			'show_in_admin_bar'     => $this->bool_show_in_admin_bar,

			/**
			 * (integer) (optional) The position in the menu order the post type should appear. show_in_menu must be true.
			 *
			 * 5 - below Posts
			 * 10 - below Media
			 * 15 - below Links
			 * 20 - below Pages
			 * 25 - below comments
			 * 60 - below first separator
			 * 65 - below Plugins
			 * 70 - below Users
			 * 75 - below Tools
			 * 80 - below Settings
			 * 100 - below second separator
			 *
			 * Default: 30 (below Comments @ 25).
			 */
			'menu_position'         => $this->int_menu_position,

			/**
			 * (string) (optional) The url to the icon to be used for this menu or the name of the icon from the iconfont.
			 *
			 * Examples:
			 * - 'dashicons-video-alt' - Uses the video icon from Dashicons[2].
			 * - 'get_template_directory_uri() . "/images/ custom-posttype-icon.png"' - Use a image located in the current theme.
			 * - 'data:image/svg+xml;base64,' . base64_encode( "<svg version = "1.1" xmlns="http:// www.w3.org/2000/svg” xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="20px" height="20px" viewBox="0 0 459 459"><path fill=”black” d=”POINTS”/></svg>” )' -  Directly embedding a svg with ‘fill=”black”‘ will allow correct colors.
			 *
			 * Default: posts' icon.
			 */
			'menu_icon'             => $this->str_menu_icon,

			/**
			 * (string or array) (optional) The string to use to build the read, edit, and delete capabilities. May be passed as
			 * an array to allow for alternative plurals when using this argument as a base to construct the capabilities,
			 * e.g., array('story', 'stories') the first array element will be used for the singular capabilities and the second
			 * array element for the plural capabilities, this is instead of the auto generated version if no array is given which
			 * would be "storys". The 'capability_type' parameter is used as a base to construct capabilities unless they are
			 * explicitly set with the 'capabilities' parameter. It seems that `map_meta_cap` needs to be set to false or null,
			 * to make this work (see note 2 below).
			 *
			 * For examples: https:// developer.wordpress.org/reference/functions/register_post_type/#capability_type
			 *
			 * - Default: 'post'
			 */
			'capability_type'       => $this->mix_capability_type,

			/*
				* (array) (optional) An array of the capabilities for this post type.
				*
				* For examples: https:// developer.wordpress.org/reference/functions/register_post_type/#capabilities
				*
				* - Default: capability_type is used to construct
				*/
			'capabilities'          => $this->arr_capabilities,

			/*
				* (boolean) (optional) Whether to use the internal default meta capability handling.
				*
				* Note: if set it to false then standard admin role can’t edit the posts types.
				* Then the edit_post capability must be added to all roles to add or edit the posts types.
				*
				* Default: null
				*/
			'map_meta_cap'          => $this->bool_map_meta_cap,

			/**
			 * (array/boolean) (optional) An alias for calling add_post_type_support() directly.
			 *
			 * As of 3.5, boolean false can be passed as value instead of an array to prevent default (title and editor) behavior.
			 *
			 * - 'title'
			 * - 'editor' (content)
			 * - 'author'
			 * - 'thumbnail' (featured image, current theme must also support post-thumbnails)
			 * - 'excerpt'
			 * - 'trackbacks'
			 * - 'custom-fields'
			 * - 'comments' (also will see comment count balloon on edit screen)
			 * - 'revisions' (will store revisions)
			 * - 'page-attributes' (menu order, hierarchical must be true to show Parent option)
			 * - 'post-formats' add post formats, see Post Formats
			 *
			 * array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes', 'post-formats' )
			 *
			 * Note: When you use custom post type that use thumbnails remember to check that the theme also supports thumbnails
			 * or use add_theme_support() function.
			 *
			 * Default: array( 'title', 'editor' )
			 */
			'supports'              => $this->mix_supports,

			/**
			 * (callback ) (optional) Provide a callback function that will be called when setting up the meta boxes for the edit form.
			 * The callback function takes one argument $post, which contains the WP_Post object for the currently edited post.
			 * Do remove_meta_box() and add_meta_box() calls in the callback.
			 *
			 * - Default: None
			 */
			'register_meta_box_cb'  => $this->str_register_meta_box_cb,

			/**
			 * (array) ( optional ) An array of registered taxonomies like category or post_tag that will be used with this post type.
			 * This can be used in lieu of calling register_taxonomy_for_object_type() directly.
			 * Custom taxonomies still need to be registered with register_taxonomy().
			 *
			 * Default: no taxonomies
			 */
			'taxonomies'            => $this->arr_taxonomies,

			/**
			 * (boolean or string)(optional) Enables post type archives. Will use $post_type as archive slug by default.
			 *
			 * Note: Will generate the proper rewrite rules if rewrite is enabled . Also use rewrite to change the slug used . if string, it should be translatable.
			 *
			 * Default: false
			 */
			'has_archive'           => $this->mix_has_archive,

			/**
			 * ( boolean or array )( optional ) Triggers the handling of rewrites for this post type . To prevent rewrites, set to false.
			 *
			 * $args array
			 * - 'slug' => string Customize the permalink structure slug. Defaults to the $post_type value. Should be translatable.
			 * - 'with_front' => bool Should the permalink structure be prepended with the front base. (example: if your permalink structure is /blog/, then your links will be: false->/news/, true->/blog/news/). Defaults to true
			 * - 'feeds' => bool Should a feed permalink structure be built for this post type. Defaults to has_archive value.
			 * - 'pages' => bool Should the permalink structure provide for pagination. Defaults to true
			 * - 'ep_mask' => const As of 3.4 Assign an endpoint mask for this post type (example). For more info see Rewrite API/add_rewrite_endpoint, and Make WordPress Plugins summary of endpoints.
			 * ----If not specified, then it inherits from permalink_epmask(if permalink_epmask is set), otherwise defaults to EP_PERMALINK.
			 *
			 * Note: If registering a post type inside of a plugin, call flush_rewrite_rules() in your activation and deactivation hook
			 * (see Flushing Rewrite on Activation below). If flush_rewrite_rules() is not used, then you will have to manually go to Settings > Permalinks and refresh your permalink structure before your custom post type will show the correct structure.
			 *
			 * - Default: true and use $post_type as slug
			 */
			'rewrite'               => $this->mix_rewrite,

			/**
			 * (string) (optional) The default rewrite endpoint bitmasks. For more info see Trac Ticket 12605 and this - Make WordPress
			 * Plugins summary of endpoints.
			 * Default: EP_PERMALINK
			 *
			 * Note: In 3.4, this argument is effectively replaced by the 'ep_mask' argument under rewrite.
			 *
			 * - Default: EP_PERMALINK
			 */
			'permalink_epmask'      => $this->str_permalink_epmask,

			/**
			 * (boolean or string) (optional) Sets the query_var key for this post type.
			 * Default: true - set to $post_type
			 *
			 * - 'false' - Disables query_var key use. A post type cannot be loaded at /?{query_var}={single_post_slug}
			 * - 'string' - /?{query_var_string}={single_post_slug} will work as intended.
			 *
			 * Note: The query_var parameter has no effect if the ‘publicly_queryable’ parameter is set to false. query_var adds the
			 * custom post type’s query var to the built-in query_vars array so that WordPress will recognize it. WordPress removes
			 * any query var not included in that array.
			 *
			 * If set to true it allows you to request a custom posts type (book) using this: example.com/?book=life-of-pi
			 *
			 * If set to a string rather than true (for example ‘publication’), you can do: example.com/?publication=life-of-pi
			 *
			 * Default: true - set to $post_type
			 */
			'query_var'             => $this->mix_query_var,

			/**
			 * (boolean) (optional) Can this post_type be exported.
			 *
			 * - Default: true
			 */
			'can_export'            => $this->bool_can_export,

			/**
			 * (boolean) (optional) Whether to delete posts of this type when deleting a user.
			 *
			 * If true, posts of this type belonging to the user will be moved to trash when then user is deleted.
			 * If false, posts of this type belonging to the user will not be trashed or deleted.
			 *
			 * If not set (the default), posts are trashed if post_type_supports('author'). Otherwise posts are not trashed or deleted.
			 *
			 * - Default: null
			 */
			'delete_with_user'      => $this->bool_delete_with_user,

			/**
			 * (boolean) (optional) Whether to expose this post type in the REST API. Must be true to enable the Gutenberg editor.
			 *
			 * - Default: false
			 */
			'show_in_rest'          => $this->bool_show_in_rest,

			/**
			 * (string) (optional) The base slug that this post type will use when accessed using the REST API.
			 *
			 * Default: $post_type (but in this class the default is null, allowing WP's default to be used).
			 */
			'rest_base'             => $this->str_rest_base,

			/**
			 * (string) (optional) An optional custom controller to use instead of WP_REST_Posts_Controller.
			 * Must be a subclass of WP_REST_Controller.
			 *
			 * Default: WP_REST_Posts_Controller
			 */
			'rest_controller_class' => $this->str_rest_controller_class,

			/**
			 * (boolean) (not for general use) Whether this post type is a native or "built-in" post_type.
			 *
			 * Note: this Codex entry is for documentation - core developers recommend you don't use this when registering your own post type
			 *
			 * Default: false
			 * 
			 * - false - default this is a custom post type
			 * - true - this is a built-in native post type (post, page, attachment, revision, nav_menu_item)
			 *
			 * - Default: false
			 */
			// '_builtin' => false,

			/**
			 * (boolean) (not for general use) Link to edit an entry with this post type.
			 *
			 * Note: this Codex entry is for documentation - core developers recommend you don't use this when registering your own post type
			 *
			 * Default: 'post.php?post=%d'
			*/
			// '_edit_link' => false,

			/*
				* For Gutenberg templates
				* https:// developer.wordpress.org/block-editor/reference-guides/block-api/block-templates/
				*/
			'template'              => $this->arr_template,
			'template_lock'         => $this->str_template_lock,
		);

		return $args;
	}

	/**
	 * Get the value of the property: mix_set_return.
	 *
	 * @return mixed
	 */
	public function getSetReturn() {

		return $this->mix_set_return;
	}

	/**
	 * Generic setter for properties that have array values.
	 *
	 * @param string    $str_prop      The property name.
	 * @param array     $arr           The array value the property is set to.
	 * @param int       $int_min_count The min length of the array.
	 * @param false|int $int_max_count The max length of the array.
	 *
	 * @return bool
	 */
	protected function setArray( string $str_prop = '', array $arr = array(), int $int_min_count = 0, $int_max_count = false ) {

		if ( property_exists( $this, $str_prop )
		&& count( $arr ) >= absint( $int_min_count )
		&& ( false === $int_max_count || count( $arr ) <= absint( $int_max_count ) ) ) {

			$this->$str_prop = $arr;
			return true;
		}
		return false;
	}

	/**
	 * Generic setter for properties that have boolean values.
	 *
	 * @param string $str_prop The property name.
	 * @param bool   $bool     The boolean value the property is set to.
	 *
	 * @return bool
	 */
	protected function setBool( string $str_prop = '', bool $bool ) {

		if ( property_exists( $this, $str_prop ) ) {

			$this->$str_prop = $bool;
			return true;
		}
		return false;
	}


	/**
	 * Generic setter for properties that have sting values.
	 *
	 * @param string $str_prop   The property to set.
	 * @param string $str        The value to set.
	 * @param array  $arr_len    An array of two keys: 'min_len' and 'max_len' (optional).
	 * @param bool   $bool_ltrim Whether to trim the string (optional).
	 *
	 * @return bool
	 */
	protected function setString( string $str_prop = '', string $str = '', array $arr_len = array(), bool $bool_ltrim = true ) {

		$arr_len_default = array(
			'min_len' => 0,
			'max_len' => false,
		);
		if ( is_array( $arr_len ) ) {

			$arr_len_default = array_merge( $arr_len_default, $arr_len );
		}

		if ( property_exists( $this, $str_prop ) ) {

			// Without this, e.g., 3x spaces would have a strlen = 3.
			if ( $bool_ltrim && empty( ltrim( $str ) ) ) {
				$str = '';
			}

			if ( strlen( $str ) >= absint( $arr_len_default['min_len'] ) && ( false === $arr_len_default['max_len'] || strlen( $str ) <= absint( $arr_len_default['max_len'] ) ) ) {

				$this->$str_prop = $str;
				return true;
			}
			return false;
		}
	}
}
